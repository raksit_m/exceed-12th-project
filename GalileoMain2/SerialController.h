#include <Arduino.h>
#include <pt.h>


//void setValue(String& serialString, char& isPeopleInRoom, int& smokeLevel, int& soundLevel);
void serialEvent(char& isPeopleInRoom, int& smokeLevel, int& soundLevel, char& smokeStatus, char& soundStatus, String& lightStatus);
void sendSerial(String sendData);
PT_THREAD(taskSendSerial(struct pt* pt, char& isPeopleInRoom, int& smokeLevel, int& soundLevel, char& smokeStatus, char& soundStatus, String& lightStatus, int& peopleDistance, int& peopleDegree));
