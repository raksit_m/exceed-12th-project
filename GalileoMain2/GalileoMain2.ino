#include <Arduino.h>
#include <pt.h>
#include <Servo.h>
#include "SerialController.h"
#include "MotionController.h"
#include "SmokeController.h"
#include "SoundController.h"
#include "LightController.h"
#include "MonitorController.h"

#define PT_DELAY(pt, ms, ts) \
  ts = millis(); \
  PT_WAIT_WHILE(pt, millis()-ts < (ms));




struct pt ptTaskSendSerial;
struct pt ptTaskMotionController;
struct pt ptTaskSmokeController;
struct pt ptTaskSoundController;
struct pt ptTaskLightController;
struct pt ptTaskMonitorController;


char isPeopleInRoom = 'N';
int smokeLevel = 0;
int soundLevel = 0;
int lightLevel = 0;
int isCalibrated = 0;
int peopleDistance = 0;
int peopleDegree = 0;
int isMonitorActive = 0;
char soundStatus = 'S';
char smokeStatus = 'S';
String lightStatus = "OFF";

Servo monitorServo;

void setup()
{
  Serial1.begin(115200);
  Serial.begin(115200);

  pinMode(MOTION_PIN, INPUT);
  pinMode(SMOKE_PIN, INPUT);
  pinMode(SOUND_PIN, INPUT);
  pinMode(LIGHT_PIN, INPUT);

  pinMode(ULTRASONIC_TRIGGER_PIN, OUTPUT);
  pinMode(ULTRASONIC_ECHO_PIN, INPUT);

  monitorServo.attach(SERVO_PIN);

  PT_INIT(&ptTaskSendSerial);
  PT_INIT(&ptTaskMotionController);
  PT_INIT(&ptTaskSmokeController);
  PT_INIT(&ptTaskSoundController);
  PT_INIT(&ptTaskLightController);
  PT_INIT(&ptTaskMonitorController);

}

///////////////////////////////////////////////////////
void loop()
{
  serialEvent(isPeopleInRoom, smokeLevel, soundLevel, smokeStatus, soundStatus, lightStatus);
  taskSendSerial(&ptTaskSendSerial, isPeopleInRoom, smokeLevel, soundLevel, smokeStatus, soundStatus, lightStatus, peopleDistance, peopleDegree);

  taskMotionController(&ptTaskMotionController, isPeopleInRoom, isMonitorActive);
  taskSmokeController(&ptTaskSmokeController, smokeLevel, smokeStatus);
  taskSoundController(&ptTaskSoundController, soundLevel, soundStatus);
  taskLightController(&ptTaskLightController, lightLevel, lightStatus);
  taskMonitorController(&ptTaskMonitorController, isCalibrated , monitorServo, peopleDistance, peopleDegree, isMonitorActive);
}
