#include <Arduino.h>
#include <pt.h>
#include "MotionController.h"

#define PT_DELAY(pt, ms, ts) \
  ts = millis(); \
  PT_WAIT_WHILE(pt, millis()-ts < (ms));


PT_THREAD( taskMotionController( struct pt* pt, char& isPeopleInRoom, int& isMonitorActive ) ) {
  static uint32_t ts;
  static uint32_t startTime;
  static uint32_t count;
  PT_BEGIN(pt);
  int motionSensorValue;
  count = 0;
  startTime = millis();
  isPeopleInRoom = 'Y';
  while (1) {
    motionSensorValue = digitalRead(MOTION_PIN);
    // Serial.println(motionSensorValue);

    if ((long)millis() - startTime < 300000 && motionSensorValue == 0) {
      count++;
    }

    if (count == 300) {
      isPeopleInRoom = 'N';
      count = 0;
    }

    if ((long)millis() - startTime >= 300000 || motionSensorValue == 1) {
      isMonitorActive = 1;
      isPeopleInRoom = 'Y';
      count = 0;
      startTime = millis();
    }

    PT_DELAY(pt, 1000, ts);
  }
  PT_END(pt);
}

