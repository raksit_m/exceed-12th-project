#include <Arduino.h>
#include <pt.h>
#include <Servo.h>
#define SERVO_PIN 11
#define ULTRASONIC_TRIGGER_PIN 6
#define ULTRASONIC_ECHO_PIN 7
#define ULTRASONIC_MAX_DISTANCE 200


PT_THREAD( taskMonitorController( struct pt* pt, int &isCalibrated ,Servo &monitorServo ,int& peopleDistance, int &peopleDegree, int& isMonitorActive) );
void calibrateServo(struct pt* pt, int *roomDistanceWithDegree, const uint32_t& maxDegreeOfRoom, Servo &monitorServo);
int shootAndGetSonicWaveInCentimeter();
