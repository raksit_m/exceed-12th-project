
#include <Wire.h>
#include <Arduino.h>
#include <pt.h>
#include <Servo.h>

#include "MonitorController.h"

#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));
    
#define MAX_ERROR_DISTANCE 3

#define MAX_DEGREE 80
#define MIN_DEGREE 30
#define SERVO_MOVE_DISTANCE 10
void shootUltraSonicWave(){
    digitalWrite(ULTRASONIC_TRIGGER_PIN, LOW);
    delayMicroseconds(2);
    digitalWrite(ULTRASONIC_TRIGGER_PIN, HIGH);
    delayMicroseconds(10);
    digitalWrite(ULTRASONIC_TRIGGER_PIN, LOW);
}

int getSonicWaveInCentimeter(){
  long duration = pulseIn(ULTRASONIC_ECHO_PIN, HIGH);
  return (int) (duration / 52);
}

int shootAndGetSonicWaveInCentimeter(){
  shootUltraSonicWave();
  return getSonicWaveInCentimeter();
}

PT_THREAD( taskMonitorController( struct pt* pt,int &isCalibrated ,Servo &monitorServo, int& peopleDistance, int &peopleDegree, int& isMonitorActive) ){
  static uint32_t ts;
  static uint32_t timeStart;
  static int count = 0;
  static const uint32_t maxDegreeOfRoom = 181;
  static int roomDistanceWithDegree[maxDegreeOfRoom] = {0};
  static int measuredCentimeter = 0;
  static int degree = 0;

  PT_BEGIN(pt);
  
    while(1){
      
      if(!isCalibrated){
        isCalibrated = 1;
       // Serial.println("Calibrate ");
        calibrateServo(pt, roomDistanceWithDegree, maxDegreeOfRoom, monitorServo);
      }else if(isMonitorActive){
        
       //  Serial.println("Scanning ");
        for(degree = MIN_DEGREE; degree < MAX_DEGREE; degree+=SERVO_MOVE_DISTANCE){
          monitorServo.write(degree);
          PT_DELAY(pt, 45, ts); // Wait Servo Move 
          
          measuredCentimeter = shootAndGetSonicWaveInCentimeter();
//          Serial.print("Centimeter : ");
//          Serial.println(measuredCentimeter);
          if(measuredCentimeter != 0 && measuredCentimeter < roomDistanceWithDegree[degree] - MAX_ERROR_DISTANCE){

            
            peopleDistance = measuredCentimeter;
            peopleDegree = degree;
            //Serial.println(String("People") + " " + String(peopleDistance) + " : " + String(peopleDegree) + " : " + String(roomDistanceWithDegree[degree]));
            degree = MAX_DEGREE - SERVO_MOVE_DISTANCE;

            timeStart = millis();
            count = 0;
          }else {
            count++;
          }
          if(count == 30) {
            isMonitorActive = 0;
          }
        }
        PT_DELAY(pt, 100, ts);
        for(; degree > MIN_DEGREE; degree--){
          monitorServo.write(degree);
          PT_DELAY(pt, 45, ts); // Wait Servo Move 
        }
      }
      PT_DELAY(pt, 20, ts);
    }
    PT_END(pt);
}

void calibrateServo(struct pt* pt, int *roomDistanceWithDegree, const uint32_t& maxDegreeOfRoom, Servo &monitorServo){
  static uint32_t ts;
  for(int degree = MIN_DEGREE; degree < MAX_DEGREE; degree+= SERVO_MOVE_DISTANCE){
    monitorServo.write(degree);
    //Serial.println("Calibrated " + String(degree));
    delay(10);
    roomDistanceWithDegree[degree] = shootAndGetSonicWaveInCentimeter();
  }
  for(int degree = MAX_DEGREE - 1; degree > MIN_DEGREE; degree--){
    monitorServo.write(degree);
    delay(10);
  }
  //Serial.println("Calibrated ");
}



