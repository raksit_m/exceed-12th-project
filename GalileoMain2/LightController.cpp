#include <Arduino.h>
#include <pt.h>
#include "LightController.h"

#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));

PT_THREAD(taskLightController(struct pt* pt, int& lightLevel, String& lightStatus)){
  static uint32_t ts;
  static uint32_t startTime;
  PT_BEGIN(pt);
  int lightSensorValue;
  lightStatus = "OFF";
  while(1){
    lightSensorValue = analogRead(LIGHT_PIN);
    lightLevel = lightSensorValue;
    if(lightLevel >= 280){
      lightStatus = "ON";
    }
    else{
      lightStatus = "OFF";
    }
    Serial.println(lightLevel);
    PT_DELAY(pt, 1000, ts);
  }
  PT_END(pt);
}

