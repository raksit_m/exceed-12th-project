#include <Arduino.h>
#include <pt.h>
#include "SmokeController.h"

#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));



PT_THREAD(taskSmokeController(struct pt* pt, int& smokeLevel, char& smokeStatus)){
  static uint32_t ts;
  static uint32_t startTime;
  static uint32_t count;
  PT_BEGIN(pt);
  int smokeSensorValue;
  count = 0;
  startTime = millis();
  smokeStatus = 'S';
  while(1){
    smokeSensorValue = analogRead(SMOKE_PIN);
    smokeLevel = smokeSensorValue; 
    // Serial.println(smokeSensorValue);

    if((long)millis() - startTime < 10000 && smokeSensorValue >= 95) {
       count++;
    }

    if(count == 10) {
      smokeStatus = 'D';
      count = 0;
    }
    
    if(count == 5) {
      smokeStatus = 'R';
    }
    
     if((long)millis() - startTime >= 10000) {
       count = 0;
       startTime = millis();
       smokeStatus = 'S';
       //continue;
    }
    
    PT_DELAY(pt, 1000, ts);
  }
  PT_END(pt);
}
