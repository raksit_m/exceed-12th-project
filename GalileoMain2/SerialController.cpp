#include <Arduino.h>
#include <pt.h>
#include <stdlib.h>
#include "SerialController.h"


#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));

void setValue(String& serialString, char& isPeopleInRoom, int& smokeLevel, int& soundLevel){
  int count = 0;
  int charStack = 0;
  char buff[64] = {0};
  
  for(int i = 0; i < serialString.length(); i++){
      if(serialString[i] == ','){
        buff[charStack++] = 0;
        switch(count){
          case 0: // isPeopleInRoom 
            isPeopleInRoom = buff[0];
            break;
          case 1: // smokeLevel
            smokeLevel = strtod(buff, 0);
            break;
           case 2: // soundLevel
            soundLevel = strtod(buff, 0);
            break;
           default:
           break;
        }
        charStack = 0;
      }else{
        buff[charStack++] = serialString[i];
      }
  }
  Serial.println("Room Status: " + String(isPeopleInRoom) + " Smoke Level: " + String(smokeLevel) + " soundLevel: " + String(soundLevel));
}

///////////////////////////////////////////////////////
void serialEvent(char& isPeopleInRoom, int& smokeLevel, int& soundLevel, char& smokeStatus, char& soundStatus, String& lightStatus) {
  if (Serial1.available() > 0) {
    String serialString = Serial1.readStringUntil('\r');
    Serial.print("value Recieve : ");
    Serial.println(serialString);
    Serial1.flush();
    // setValue(serialString ,isPeopleInRoom, smokeLevel, soundLevel);
  }
}

///////////////////////////////////////////////////////
void sendSerial(String sendData){
  Serial1.print(sendData);
  Serial1.print('\r');
  Serial.println(sendData);
  Serial.print('\r');
}

PT_THREAD(taskSendSerial(struct pt* pt, char& isPeopleInRoom, int& smokeLevel, int& soundLevel, char& smokeStatus, char& soundStatus, String& lightStatus, int& peopleDistance, int& peopleDegree))
{
  static uint32_t ts;
  PT_BEGIN(pt);
  
  while (1)
  {
    sendSerial(String(isPeopleInRoom) + "," + String(smokeLevel) + "," + String(soundLevel) + "," + String(smokeStatus) + "," + String(soundStatus) + "," + String(lightStatus) + "," + String(peopleDistance) + "," + String(peopleDegree));
    PT_DELAY(pt, 600, ts);
  }
  PT_END(pt);
}

