#include <Arduino.h>
#include <pt.h>
#include "SoundController.h"


#define PT_DELAY(pt, ms, ts) \
    ts = millis(); \
    PT_WAIT_WHILE(pt, millis()-ts < (ms));



PT_THREAD(taskSoundController(struct pt* pt, int& soundLevel, char& soundStatus)){
  static uint32_t ts;
  static uint32_t startTime;
  static uint32_t count;
  PT_BEGIN(pt);
  int soundSensorValue;
  count = 0;
  startTime = millis();
  soundStatus = 'S';
  
  while(1){
    soundSensorValue = analogRead(SOUND_PIN); 
    // Serial.println(smokeSensorValue);
    soundLevel = soundSensorValue;

    if((long)millis() - startTime < 30000 && soundSensorValue >= 250) {
       count++;
    }

    if(count == 20) {
      soundStatus = 'D';
      count = 0;
    }
    
    if(count == 10) {
      soundStatus = 'R';
    }
    
     if((long)millis() - startTime >= 30000) {
       count = 0;
       startTime = millis();
       soundStatus = 'S';
       //continue;
    }
    
    PT_DELAY(pt, 1000, ts);
  }
  
  PT_END(pt);
}

